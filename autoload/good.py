# ============================================================================
# Description: TODO
# Maintainer:  Vojtech Rylko <vojta.rylko@gmail.com>
# License:     TODO
# Notes:       TODO
#
# ============================================================================

import vim
import datetime
import os

def good_copyright( filename ):
    extensions = \
        { '%' : [ 'erl' ]
        , '#' : [ 'pm', 't', 'py' ]
        , '//' : [ 'java', 'cpp', 'c' ]
        , '--' : [ 'sql' ]
        }
    comment = ''
    for ( ext_comment, exts ) in extensions.iteritems():
        if any( map( lambda ext: filename.endswith( '.' + ext ), exts ) ):
            comment = ext_comment
            break
    year = datetime.datetime.now().year
    copyright = '%s Copyright (C) 2007-%d, GoodData(R) Corporation. All rights reserved.' % \
        ( comment, year )
    return copyright

def dumper( nrow, line, filename ):
    extensions = \
        { _erlang_dumper: [ 'erl' ]
        , _perl_dumper: [ 'pm', 't' ]
        }
    lang_dumper = None
    for ( dumper_, exts ) in extensions.iteritems():
        if any( map( lambda ext: filename.endswith( '.' + ext ), exts ) ):
            lang_dumper = dumper_

    if lang_dumper:
        return lang_dumper( nrow, line, filename )
    else:
        return ( 'error: unkown file type "%s"' % filename, 0 )

def _perl_dumper( nrow, line, filename ):
    var = line.strip().rstrip( ';' )
    dumper = "%suse Data::Dumper; print 'DEBUG(%s:%d): %s = ' . Dumper( %s );\n" % \
        ( _indentation( line ), filename, nrow, var.replace( "'", "\\'" ), var )
    return ( dumper, len( dumper ) - 4 - len( var ) )

def _erlang_dumper( nrow, line, filename ):
    var = line.strip().rstrip( ';,.');
    dumper = "%sio:format( standard_error, \"DEBUG(~p:~p): %s = ~p~n\", [ ?MODULE, ?LINE, %s ] )," % \
        ( _indentation( line ), var.replace( '"', '\\"' ), var )
    return ( dumper, len( dumper ) - 5 - len( var ) )

ABBRS = {
    'JS' : ( 'Jiri Schmid',  'jiri.schmid@gooddata.com' ),
    'DKU' : ( 'David Kubecka', 'david.kubecka@gooddata.com' ),
    'VRY' : ( 'Vojtech Rylko', 'vojtech.rylko@gooddata.com' ),
}

def review( _, abbr ):
    ( name, mail ) = ABBRS.get( abbr, ('', '') )
    return 'Reviewed-by: %s <%s>' % ( name, mail )


def _indentation( line ):
    indentation = line[ 0 : len( line ) - len( line.lstrip() ) ]
    if indentation.startswith( '\t' ):
        indentation = indentation.rstrip( ' ' )
    return indentation

