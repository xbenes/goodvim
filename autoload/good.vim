" ============================================================================
" Description: TODO
" Maintainer:  Vojtech Rylko <vojta.rylko at gmail dot com>
" License:     TODO
"
" ============================================================================

" Insert copyright before current line.
" Usage:
" At begining of the file (line 1) run command :GoodCopyright.
" The line 1 does not have to be empty - will not be replaced.
function! good#GoodCopyright()
    python insert_line( good_copyright )
endfunction

" Get current line and print it with Data::Dumper.
" This is Perl-only. TODO: Make Erlang version.
" Usage:
" You want to print value of $struct from line: my $struct = { x => 1 };
" so go to empty line and write $struct and call command :GoodDump.
function! good#GoodDump()
    python current_line_apply( dumper )
endfunction


function! good#GoodReview( abbr )
    python insert_line( review, "abbr" )
endfunction

let s:plugin_path = escape(expand('<sfile>:p:h'), '\')
exe 'pyfile ' . s:plugin_path . '/good_vim.py'
exe 'pyfile ' . s:plugin_path . '/good.py'
