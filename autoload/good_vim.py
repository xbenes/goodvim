# ============================================================================
# Description: general functions
# Maintainer:  Vojtech Rylko <vojta.rylko@gmail.com>
# License:     TODO
# Notes:       TODO
#
# ============================================================================



def insert_line( func, *args ):
    line = func( vim.current.buffer.name, *eval_args( *args ) )
    ( nline, _ ) = vim.current.window.cursor
    vim.current.window.buffer.append( line, nline - 1 )

def eval_args( *args ):
    return map( lambda arg: vim.eval( "a:%s" % arg ), args )

# func - ( nline, line, filename ) -> ( new_line, new_col )
def current_line_apply( func ):
    ( nline, col, line ) = _current_line()
    filename = vim.current.buffer.name.split( os.sep )[ -1 ]
    ( new_line, var_col ) = func( nline, line, filename )
    vim.current.window.buffer[ nline - 1 ] = new_line
    # put cursor to start of $var
    vim.current.window.cursor = ( nline, var_col )

def _current_line():
    ( nline, col ) = vim.current.window.cursor
    line = vim.current.window.buffer[ nline - 1 ]
    return ( nline, col, line )

