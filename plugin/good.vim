" ============================================================================
" Description: TODO
" Maintainer:  Vojtech Rylko <vojta.rylko at gmail dot com>
" License:     TODO
"
" ============================================================================

if exists('loaded_good_vim')"{{{
    finish
endif
let loaded_good_vim = 1"}}}

command! -nargs=0 GoodCopyright call good#GoodCopyright()
command! -nargs=0 GoodDump call good#GoodDump()
command! -nargs=1 GoodReview call good#GoodReview( <f-args> )
